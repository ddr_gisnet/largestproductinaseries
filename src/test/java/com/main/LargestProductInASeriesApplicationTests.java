package com.main;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.main.LargestProductInASeriesApplication;
import com.service.ICalculoService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LargestProductInASeriesApplication.class)
public class LargestProductInASeriesApplicationTests {
	
	@Autowired
	private ICalculoService calculoService;

	@Test
	public void contextLoads() {
		calculoService.calcularProductoEnSerie();
	
	}

}
