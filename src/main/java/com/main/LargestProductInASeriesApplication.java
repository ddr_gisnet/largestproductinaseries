package com.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.service.ICalculoService;

@SpringBootApplication(scanBasePackages= {"com.*"})
public class LargestProductInASeriesApplication implements CommandLineRunner {
	
	private   Logger logger = LoggerFactory.getLogger(LargestProductInASeriesApplication.class);
	
	@Autowired
	private ICalculoService calculoService;

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(LargestProductInASeriesApplication.class);
		//app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
		
	}

	@Override
	public void run(String... args) throws Exception {
		logger.debug("<-- inicio -->");
		calculoService.calcularProductoEnSerie();
		 logger.debug("<-- fin -->");
		
	}

	
}
