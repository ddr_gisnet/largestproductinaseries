package com.model;

import java.util.List;

public class Case {
	
	private Integer N;
	private Integer K;
	private Long D;
	private String R;
	
	private List<Long> listaResultadoCombinacion;
	
	public Integer getN() {
		return N;
	}
	public void setN(Integer n) {
		N = n;
	}
	public Integer getK() {
		return K;
	}
	public void setK(Integer k) {
		K = k;
	}
	
	public Long getD() {
		return D;
	}
	public void setD(Long d) {
		D = d;
	}
	public String getR() {
		return R;
	}
	public void setR(String r) {
		R = r;
	}
	public List<Long> getListaResultadoCombinacion() {
		return listaResultadoCombinacion;
	}
	public void setListaResultadoCombinacion(List<Long> listaResultadoCombinacion) {
		this.listaResultadoCombinacion = listaResultadoCombinacion;
	}
	
	
	

}
