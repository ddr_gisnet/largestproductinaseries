package com.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.model.Case;
import com.service.ICalculoService;

@Service
public class CalculoService implements ICalculoService {
	
	private   Logger logger = LoggerFactory.getLogger(CalculoService.class);
	
	private Integer T;
	private Integer N,K;
	private Long D;
	private Boolean salir=Boolean.FALSE;
	
	private List<Case> listCase;
	

	@Override
	public void calcularProductoEnSerie() {
			logger.debug("<-- inicio -->");
			
			Scanner scanner = new Scanner(System.in);
			
			listCase=new ArrayList<>();
			
			logger.debug("Teclea x en cualquier momento para salir");
			do {
				
				do {
					logger.debug("Introduce el valor para T: ");
					String tStr = scanner.nextLine();
					if (tStr.trim().equalsIgnoreCase("x")) {
						salir=Boolean.TRUE;
					}else {
						try {
							T=Integer.parseInt(tStr);
							if (T<1 || T> 100) {
								String msnError="T no puede ser menor que 0 o mayor que 100";
								throw new Exception(msnError);
							}
							break;
						} catch (Exception e) {
							logger.debug("Valor no valido " + e.getMessage());
						}
					}
				}while(!salir && true );
				
				if (salir) {
					break;
				}
				
				for(int i=0;i<T;i++) {
					
					Case caso = new Case(); 
					
					do {
						logger.debug("valores para caso " + i);
						logger.debug("Introduce el valor para N y K: ");
						String nykStr = scanner.nextLine();
						if (nykStr.trim().equalsIgnoreCase("x")) {
							salir=Boolean.TRUE;
						}else {
							try {
								String arregloTemp[]=nykStr.split(" ");
								if (arregloTemp.length!=2) {
									String msnError="el formato no es correcto";
									throw new Exception(msnError);
								}
								
								N=Integer.parseInt(arregloTemp[0]);
								
								K=Integer.parseInt(arregloTemp[1]);
								
								if (K > 7 || K < 1) {
									String msnError="K no puede ser mayor que 7 o menor que 0";
									throw new Exception(msnError);
								}
								
								if (N > 1000 || N < K) {
									String msnError="N no puede ser mayor que 1000 o menor que " + K;
									throw new Exception(msnError);
								}
								
								caso.setN(N);
								caso.setK(K);
								
								break;
							} catch (Exception e) {
								logger.debug("Valor no valido " + e.getMessage());
							}
						}
					}while(!salir && true );
					
					if (salir) {
						break;
					}
					
					do {
						logger.debug("Introduce el valor para el digito: ");
						String dStr = scanner.nextLine();
						if (dStr.trim().equalsIgnoreCase("x")) {
							salir=Boolean.TRUE;
						}else {
							try {
								
								if (dStr.length() > N) {
									String msnError="la longitud del digito no puede ser mayor que " + N;
									throw new Exception(msnError);
								}
								
								D=Long.parseLong(dStr);
								
								caso.setD(D);
								
								break;
							} catch (Exception e) {
								logger.debug("Valor no valido " + e.getMessage());
							}
						}
					}while(!salir && true);
					
					if (salir) {
						break;
					}	
					
					listCase.add(caso);
				}
				
				if (salir) {
					break;
				}
				
				
				this.hacerCalculo(listCase);
				
				this.imprimirResultado(listCase);
				
				
				
			}while(true);
			
			
			 scanner.close();
			
			logger.debug("<-- fin -->");

	}
	
	private void hacerCalculo(List<Case> listCase) {
		logger.debug("<-- inicio -->");
		
		for (Case elcaso : listCase) {
			
				int i = 0;
			
				elcaso.setListaResultadoCombinacion(new ArrayList<>());
				
				while( (i+elcaso.getK()) <= elcaso.getD().toString().length() ) {
					
					String combinacion=elcaso.getD().toString().substring(i, (i+elcaso.getK()) );
					
					char arregloCharTemp[]=combinacion.toCharArray();
					
					Long resultadoCombinacion = Long.valueOf( String.valueOf(arregloCharTemp[0]));
					
					for (int j = 1; j < arregloCharTemp.length; j++) {
						resultadoCombinacion*= Long.valueOf( String.valueOf(arregloCharTemp[j]));
					}
					
					elcaso.getListaResultadoCombinacion().add(resultadoCombinacion);
					
					i++;
				}

				
		}
		
		logger.debug("<-- fin -->");
		
	}
	
	private void imprimirResultado(List<Case> listCase) {
		logger.debug("<-- inicio -->");
		
		listCase.stream().forEach (elcaso -> logger.debug("resultado:"+elcaso.getListaResultadoCombinacion().stream().max( (res0, res1) -> Long.compare(res0, res1) ).get().toString()) );
		
		logger.debug("<-- fin -->");
		
	}

}
